import React from 'react'
import './AboutMe.scss';
import profilePicture from '../../images/MN_Profil.png';
import brainPicture from '../../images/brain.jpg';

const AboutMe = () => {
    return (
        <div className="about-me">
            <img className="about-me__profile-picture" src={profilePicture} alt="profilbild"/>
            <div className="about-me__skills">
                <h1 className="about-me__heading">Maximilian Neitzel</h1>
                <h2 className="about-me__heading">Fullstack Developer</h2>
                <ul className="about-me__skills-list">
                    <li className="about-me__skills-item">Java</li>
                    <li className="about-me__skills-item">C#</li>
                    <li className="about-me__skills-item">JavaScript</li>
                </ul>
            </div>
        </div>
    )
}

export default AboutMe
