import React from 'react'
import { useForm } from "react-hook-form";
import './Contact.scss';

const Contact = () => {

    const { register, handleSubmit, errors } = useForm();
    // Handler

    const onFormSubmit = data => console.log(data);

    return (
        <div className="contact">
            <h1>Kontakt</h1>
            <form className="contact__form" onSubmit={handleSubmit(onFormSubmit)}>
                <label htmlFor="name" >Name: </label>
                <input name="name" type="text" ref={register({ required: true })} />
                {errors.name && <span>This field is required</span>}
                <label htmlFor="email">Email: </label>
                <input name="email" type="email" ref={register({ required: true })} />
                {errors.email && <span>This field is required</span>}
                <label htmlFor="message">Ihre Nachricht: </label>
                <textarea name="message" type="text" ref={register({ required: true })} />
                {errors.message && <span>This field is required</span>}
                <button type="submit">Absenden</button>
            </form>
        </div>
    )
}

export default Contact
