import React from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import AboutMe from './pages/AboutMe/AboutMe';
import Home from './pages/Home/Home';
import Projects from './pages/Projects/Projects';
import Contact from './pages/Contact/Contact';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  const navItems = [
    {
      title : "Home",
      link : "/home"
    },
    {
      title : "Über mich",
      link : "/about-me"
    },
    {
      title : "Projekte",
      link : "/projects"
    },
    {
      title : "Kontakt",
      link : "/contact"
    }
  ];

  return (
    <div className="App">
    <Router>
      <Navbar items={navItems} />
      <Switch>
        <Route exact path="/home">
          <Home />
        </Route>
        <Route exact path="/about-me">
          <AboutMe />
        </Route>
        <Route exact path="/projects">
          <Projects />
        </Route>
        <Route exact path="/contact">
          <Contact />
        </Route>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
