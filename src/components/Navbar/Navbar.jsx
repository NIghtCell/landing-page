import React from 'react'
import './Navbar.scss';
import { NavLink } from 'react-router-dom';
import { useMedia } from 'react-media';
import NavbarIcon from '../NavbarIcon/NavbarIcon';

const Navbar = ({items}) => {
    const matches = useMedia({queries: {
        small: "(max-width: 779px)",
        medium: "(min-width: 780px)",
    }})
    const [showNavigation, setShowNavigation] = React.useState(matches.medium);
    React.useEffect(() => {
        setShowNavigation(matches.medium);
    }, [matches.medium])

    const navItems = items.map(item => (
            <li onClick={() => matches.small && setShowNavigation(false)} className="navbar__item"><NavLink to={item.link} activeClassName="selected" className="navbar__item_link">{item.title}</NavLink></li>
        ));
    const isMobile = matches.small;

    return (
        <>
            {isMobile && <NavbarIcon isOpen={showNavigation} onClick={() => setShowNavigation(!showNavigation)} />}
            {showNavigation && (
                <nav className="navbar">
                <ul className="navbar__list">
                    {navItems}
                </ul>
            </nav>
            )}
        </>
    )
}

export default Navbar;
