import React from 'react'
import './NavbarIcon.scss';
import classNames from 'classnames';

const NavbarIcon = ({onClick, isOpen}) => {
    const navbarClasses = classNames({
        'navbar__icon' : true,
        'isOpen' : isOpen,
    });

    return (
        <div onClick={onClick}
            className={navbarClasses}>
            <div className="navbar__icon__bar1" />
            <div className="navbar__icon__bar2" />
            <div className="navbar__icon__bar3" />
        </div>
    )
}

export default NavbarIcon
